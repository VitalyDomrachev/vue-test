export default {
  recaptchaSecret: '6Le48kQUAAAAAENKWEo13O1cZ9J1he_9_L3RZmyM',
  recaptchaUrl: 'https://www.google.com/recaptcha/api/siteverify',
  umbracoUsr: 'restapiuser@user.com',
  umbracoExamineUrl: 'umbraco/BackOffice/Api/ExamineManagementApi/GetSearchResults',
  umbracoPwd: 'restapipassword',
  umbracoHost: 'http://10.201.223.103:1213/',
  umbracoAuthUrl: 'umbraco/oauth/token',
  umbracoSearchUrl: 'umbraco/rest/v1/content/published/search',
  moexFileHost: 'http://fs.moex.com/content/annualreports/',
  umbracoAddReportService: 'umbraco/competition/issues/add',
  currentYear: '2017',
  startDate: '2018-04-10',
  endDate: '2018-04-20'
}
