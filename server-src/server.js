import path from 'path'
import express from 'express'
import bodyParser from 'body-parser'
import compression from 'compression'
import {initializeUmbraco} from './utils/umbracoService'
import {initializeServices} from './services/globalService'
console.log('starting server')
const app = express()
var DIST_DIR
var HTML_FILE
console.log('process.env.NODE_ENV = ' + process.env.NODE_ENV)
if (process.env.NODE_ENV === 'debug') {
  HTML_FILE = path.join(__dirname, '../dist/index.html')
  DIST_DIR = path.join(__dirname, '../dist')
} else if (process.env.NODE_ENV === 'IIS') {
  HTML_FILE = path.join(__dirname, 'index.html')
  DIST_DIR = path.join(__dirname, 'static')
} else {
  HTML_FILE = path.join(__dirname, 'index.html')
  DIST_DIR = path.join(__dirname, '')
}
console.log('html: ' + HTML_FILE + '\r\n dist ' + DIST_DIR)
const DEFAULT_PORT = 8080

function initialize (app) {
  app.use(bodyParser.json())
  console.log(bodyParser.json())
  app.set('port', process.env.PORT || DEFAULT_PORT)
  initializeUmbraco(app)
  initializeServices(app)
}
if (process.env.NODE_ENV !== 'debug') {
  app.use(express.static(DIST_DIR))
  app.use(compression())
  initialize(app)
  app.get('*', (req, res) => res.sendFile(HTML_FILE))

  app.listen(app.get('port'))
  console.log('listen' + app.get('port'))
}

process.on('unhandledRejection', function (reason, p) {
  console.error(reason)
})
process.on('uncaughtException', function (error) {
  console.error(error)
})
export {initialize}
