import settings from '../settings.js'
import axios from 'axios'
import {getYears} from '../services/years'

let instance = axios.create({
  baseURL: settings.umbracoHost
})

const token = {
  access_token: undefined,
  token_type: undefined,
  expires_in: undefined,
  requested: false
}

function initializeUmbraco (app) {
  console.log('initializeUmbraco')
  requestToken().then(result => {
    if (result.success) getYears()
  })
}

async function requestToken () {
  let authData = 'grant_type=password&username=' + settings.umbracoUsr + '&password=' + settings.umbracoPwd

  try {
    let result = await instance.post(settings.umbracoAuthUrl, authData)
    console.log('Result from Umbraco: ')
    console.log(result)
    if (!result.data.error) {
      console.log('token received ')
      console.log(result.data)
      token.access_token = result.data.access_token
      token.token_type = result.data.token_type
      token.expires_in = result.data.expires_in
      token.requested = true
      instance.defaults.headers.common['Authorization'] = 'Bearer ' + token.access_token
      console.log(token)
      return { success: true }
    } else {
      console.log('error from Umbraco ' + result.data.error + result.data.error_description)
    }
  } catch (err) {
    console.log('error from Umbraco ')
    console.error(err)
  }
  token.requested = false
  token.access_token = ''
  token.token_type = ''
  token.expires_in = ''
  return { success: false, error: 'Can not request token' }
}

async function getToken () {
  return { success: true, data: token }
}

async function search (luceneExpression, isExamine = false) {
  return apiCall(() => { return searchFunction(luceneExpression, isExamine) })
}

async function searchFunction (luceneExpression, isExamine) {
  console.log(luceneExpression)
  try {
    let numberOfPages = 1
    let results = []
    // nodeTypeAlias:Juror AND path:\-1*1068,* AND parentID:1067
    if (isExamine) { // searcherName=ExternalSearcher&query=nodeTypeAlias:Documents&queryType=lucene
      let response = await instance.get(settings.umbracoExamineUrl, {
        params: {
          searcherName: 'ExternalSearcher', query: luceneExpression, queryType: 'lucene'
        }
      })
      results = response.data
    } else {
      let page = 0
      while (page < numberOfPages) {
        let result = await instance.get(settings.umbracoSearchUrl, { params: { lucene: luceneExpression, pageIndex: page } })
        if (!result.data.error) {
          numberOfPages = result.data.totalPages
          page++
          if (result.data._embedded && result.data._embedded.content) {
            results.push(result.data._embedded.content)
          }
        } else {
          console.log('search error ' + result.data.error + result.data.error_description)
          return { success: false, error: 'unable to execute search of ' + luceneExpression }
        }
      }
      results = [].concat(...results)
    }
    return { success: true, data: results }
  } catch (err) {
    console.error(err)
    if (err.response && err.response.status === 401) {
      let result = await requestToken()
      if (result.success) {
        return searchFunction(luceneExpression, isExamine)
      }
    }
    return { success: false, error: 'unable to execute search of ' + luceneExpression }
  }
}

function createObjectFromTemplate (str) {
  var regex1 = /(?:(?:<br>)?content_splitter\sid=)([^<]*)<br>/

  let arr = str.split(regex1)
  console.log(arr)
  let obj = {}
  for (var i = 1; i + 1 < arr.length; i += 2) {
    obj[arr[i]] = arr[i + 1]
  }
  return obj
}

async function getContent (url) {
  try {
    console.log('get content: ' + url)
    let result = await instance.get(encodeURI(url))
    console.log('get result:')
    if (result.status === 200) {
      return { success: true, data: createObjectFromTemplate(result.data) }
    } else {
      console.log('search error ' + result.statusText)
      return { success: false, error: 'unable to get content ' + url }
    }
  } catch (err) {
    return { success: false, error: err }
  }
}

async function addIssue (issue) {
  try {
    console.log('add issue: ' + JSON.stringify(issue))
    let result = await instance.post(settings.umbracoAddReportService, issue)
    console.log('add result: ' + result.status)
    if (result.status === 200) {
      return true
    } else {
      console.log('search error ' + result.statusText)
      return false
    }
  } catch (err) {
    console.log(err)
    return false
  }
}

async function apiCall (apiFunction) {
  try {
    let tokenResult = await getToken()
    if (!tokenResult.success) {
      return { success: false, error: 'Unable to get token' }
    }
    return apiFunction()
  } catch (err) {
    console.error(err)
    if (err.response.status === 401) {
      console.log('response status 401, trying to renew token')
      let result = await requestToken()
      if (result.success) {
        return apiFunction()
      }
    }
  }
  return { success: false, error: 'unable to execute Umbraco call' }
}

export { initializeUmbraco, search, getContent, addIssue }
