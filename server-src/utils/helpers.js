let groupBy = function (xs, ...keys) {
  let key = keys.shift()
  let reduced = xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x)
    return rv
  }, {})
  if (keys.length > 0) {
    for (key in reduced) {
      reduced[key] = groupBy(reduced[key], keys)
    }
  }
  return reduced
}

let parseUmbracoJson = function (str) {
  return JSON.parse(str.replace(/'/g, '"').replace(/(['"])?([a-z0-9A-Z_]+)(['"])?:/g, '"$2": '))
}

export { groupBy, parseUmbracoJson }
