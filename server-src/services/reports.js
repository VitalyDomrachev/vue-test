import { search, addIssue } from '../utils/umbracoService'
import { getYears } from './years'
import { validate } from './gcaptcha'
import settings from '../settings'
import { parseUmbracoJson } from '../utils/helpers'

function initializeReports (app) {
  app.get('/getReports', getReports)
  app.post('/addReport', addReport)
}

async function getReports (req, res) {
  let year = req.query.year ? req.query.year : settings.currentYear

  let yearId = (await getYears()).find(item => item.year === year).id
  console.log('Get reports for: ' + year + ' - year ')

  let result = await search(`nodeTypeAlias:Issue AND IsActive:1 AND path:\\-1,*,${yearId},*`, true)
  if (!result.success) {
    res.status(500).json({
      error: 'unable to get reports for: ' + year + ' - year '
    })
  }
  let getSizeMB = function (size) {
    return Math.round(size * 100 / 1048576) / 100
  }
  let formatUrl = function (url) {
    if (!url) {
      return undefined
    }
    if (url.trim().substring(0, 4) === 'http') {
      return url
    }
    return 'http://' + url
  }
  let getDocumentUrl = function (fileId, fileType, fileName, fileExtension) {
    return `${settings.moexFileHost}${fileId}/${fileType}/${fileName}.${fileExtension}`
  }

  result = result.data.map(item => {
    let fields = item.Fields
    let companyInfo = fields.PrimaryCompanyName ? JSON.parse(fields.PrimaryCompanyName) : undefined
    let company = (companyInfo && companyInfo.length > 0)
      ? {name: companyInfo[0].label, id: companyInfo[0].key}
      : undefined
    return {
      year: year,
      image: fields.Cover ? settings.umbracoHost + parseUmbracoJson(fields.Cover).src : undefined,
      image2x: fields.Cover ? settings.umbracoHost + parseUmbracoJson(fields.Cover).src : undefined,
      title: fields.OpenName,
      company: company,
      companyLink: {
        link: formatUrl(fields.WebSite),
        text: fields.ShortName
      },
      text: fields.__Raw_PreviewText || fields.PreviewText,
      documents: {
        ru: fields.RusReportIsActive === '1'
          ? {
            label: 'RU',
            link: fields.RusReportIsFile === '1'
              ? getDocumentUrl(fields.IssuerID, '1', fields.RusReportFileName, fields.RusReportFileExtension)
              : formatUrl(fields.RusReportURL),
            sizeMB: getSizeMB(fields.RusReportFileSize),
            format: fields.RusReportFileExtension,
            contentType: fields.RusReportContentType
          }
          : undefined,
        en: fields.EngReportIsActive === '1'
          ? {
            label: 'EN',
            link: fields.EngReportIsFile === '1'
              ? getDocumentUrl(fields.IssuerID, '2', fields.EngReportFileName, fields.EngReportFileExtension)
              : formatUrl(fields.EngReportURL),
            sizeMB: getSizeMB(fields.EngReportFileSize),
            format: fields.EngReportFileExtension,
            contentType: fields.EngReportContentType
          }
          : undefined,
        socEn: fields.SocialEngReportIsActive === '1'
          ? {
            label: 'Soc EN',
            link: fields.SocialEngReportIsFile === '1'
              ? getDocumentUrl(fields.IssuerID, '4', fields.SocialEngReportFileName, fields.SocialEngReportFileExtension)
              : formatUrl(fields.SocialEngReportURL),
            sizeMB: getSizeMB(fields.SocialEngReportFileSize),
            format: fields.SocialEngReportFileExtension,
            contentType: fields.SocialEngReportContentType
          }
          : undefined,
        socRu: fields.SocialRusReportIsActive === '1'
          ? {
            label: 'Soc RU',
            link: fields.SocialRusReportIsFile === '1'
              ? getDocumentUrl(fields.IssuerID, '3', fields.SocialRusReportFileName, fields.SocialRusReportFileExtension)
              : formatUrl(fields.SocialRusReportURL),
            sizeMB: getSizeMB(fields.SocialRusReportFileSize),
            format: fields.SocialRusReportFileExtension,
            contentType: fields.SocialRusReportContentType
          }
          : undefined,
        interactive: fields.OnlineReportIsActive === '1'
          ? {
            label: 'Интерактивный отчет',
            link: formatUrl(fields.OnlineEngReportURL)
          }
          : undefined
      },
      reportHtml: fields.__Raw_Description || fields.Description,
      images: [1, 2, 3, 4, 5].map(id => {
        let previewJson = fields['Preview' + id]
        return previewJson ? {
          image: settings.umbracoHost + parseUmbracoJson(previewJson).src
        } : undefined
      }).filter(image => image),
      nominations: fields.Nominations ? JSON.parse(fields.Nominations).map(i => i.key) : []
    }
  })
  res.send(JSON.stringify(result))
}

async function addReport (req, res) {
  console.log('Add report request')
  let today = new Date()
  let isReportAddAvailable = new Date(settings.startDate) < today && new Date(settings.endDate) > today
  if (!isReportAddAvailable) {
    res.send(false)
    return
  }

  let captcha = req.body.captcha
  try {
    let result = await validate(captcha)
    if (result) {
      let report = req.body.report
      console.log('Validated captcha and got report: ' + JSON.stringify(report))

      res.send(await addIssue(report))
    } else {
      console.log('Failed to validate captcha ' + captcha)
      res.send(false)
    }
  } catch (err) {
    console.log('Failed to validate captcha ' + captcha)
    res.send(false)
  }
}

export {
  initializeReports
}
