import { search } from '../utils/umbracoService'
import { getYears } from './years'
import { parseUmbracoJson } from '../utils/helpers'
import settings from '../settings'

function initializeJury (app) {
  app.get('/getJury', getJury)
}

async function getJury (req, res) {
  console.log('Get jury')
  let results = await Promise.all([search('nodeTypeAlias:Jury', true), search('nodeTypeAlias:Juror', true)])
  if (!results[0].success || !results[1].success) {
    res.status(500).json({
      error: 'unable to get Jury'
    })
  }
  let juryArchiveResult = results[0].data
  let juryResult = results[1].data
  let juryObj = {}
  let years = await getYears()
  for (let yearId in years) {
    let year = years[yearId]
    let juryForYear = juryArchiveResult.find(item => item.Fields.parentID === year.id)
    if (juryForYear) {
      let juryYear = year.year
      juryObj[juryYear] = {
        archiveTitle: juryForYear.Fields.JuryTitle,
        title1: juryForYear.Fields.Title1,
        title2: juryForYear.Fields.Title2,
        description: juryForYear.Fields.Description,
        jury: juryResult.filter(item => item.Fields.parentID === juryForYear.Fields.id).map((item) => {
          return {
            firstName: item.Fields.FirstName,
            middleName: item.Fields.MiddleName,
            lastName: item.Fields.LastName,
            title: item.Fields.Title,
            image: item.Fields.Image
              ? settings.umbracoHost + parseUmbracoJson(item.Fields.Image).src
              : '/static/porTRAIT2.jpg'
          }
        })
      }
    }
  }
  res.json(juryObj)
}

export {
  initializeJury
}
