import { search } from '../utils/umbracoService'

let years

async function getYears () {
  if (!years) {
    console.log('Get years')
    let response = await search('nodeTypeAlias:Year', true)
    if (!response.success) {
      console.error('unable to get Years')
    }
    years = response.data.map(item => {
      return {
        year: item.Fields.nodeName,
        id: item.Fields.id
      }
    })
  }
  return years
}
export {getYears}
