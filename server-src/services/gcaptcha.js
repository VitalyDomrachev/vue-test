import settings from '../settings.js'
import axios from 'axios'
import https from 'https'

async function validate (gRecaptchaResponse) {
  console.log('Captcha: received response = ' + gRecaptchaResponse)
  try {
    let response = await axios({
      method: 'post',
      url: settings.recaptchaUrl,
      params: {
        secret: settings.recaptchaSecret,
        response: gRecaptchaResponse
      },
      httpsAgent: new https.Agent({
        rejectUnauthorized: false
      })
    })
    let data = response.data
    if (data.success !== undefined && !data.success) {
      console.log('failed to check captcha. Invalid response')
      return false
    } else {
      console.log('Validated captcha. Valid response')
      return true
    }
  } catch (err) {
    console.log('Error from google captcha: ' + err)
    return false
  }
}

export {validate}
