import { search } from '../utils/umbracoService'
import { getYears } from './years'

function initializeAbout (app) {
  app.get('/getAbout', getAbout)
}

async function getAbout (req, res) {
  let years = await getYears()
  let currentYear = (new Date()).getFullYear()
  let currentYearId = years.find(item => Number.parseInt(item.year) === currentYear).id
  console.log('Get About')
  let aboutResult = await search('nodeTypeAlias:AboutKonkurs AND parentID:' + currentYearId, true)
  if (!aboutResult.success) {
    res.status(500).json({
      error: 'unable to get About'
    })
  }
  if (aboutResult.data && aboutResult.data.length && aboutResult.data.length === 1) {
    res.json({
      htmlContent: aboutResult.data[0].Fields.__Raw_htmlcontent || aboutResult.data[0].Fields.htmlcontent,
      title: aboutResult.data[0].Fields.title
    })
  } else {
    res.status(404).json({
      error: 'unable to get About. No data'
    })
  }
}

export {initializeAbout}
