import {initializeNominations} from './nominations'
import {initializeCommission} from './commission'
import {initializeDocuments} from './documents'
import {initializeJury} from './jury'
import {initializeReports} from './reports'
import {initializeCeremony} from './ceremony'
import {initializeStaticContent} from './staticContent'
import {initializeContacts} from './contacts'
import {initializeAbout} from './about'
import {initializeCompetition} from './competition'

function initializeServices (app) {
  initializeNominations(app)
  console.log('Initialized nominations')
  initializeCommission(app)
  console.log('Initialized commission')
  initializeDocuments(app)
  console.log('Initialized documents')
  initializeJury(app)
  console.log('Initialized jury')
  initializeReports(app)
  console.log('Initialized reports')
  initializeCeremony(app)
  console.log('Initialized ceremony')
  initializeStaticContent(app)
  console.log('Initialized static content')
  initializeContacts(app)
  console.log('Initialized contacts')
  initializeAbout(app)
  console.log('Initialized about')
  initializeCompetition(app)
  console.log('Initialized competition')
}

export {initializeServices}
