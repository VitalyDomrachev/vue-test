import { search } from '../utils/umbracoService'
import { getYears } from './years'
import settings from '../settings'

function initializeCommission (app) {
  app.get('/getCommission', getCommission)
}

async function getCommission (req, res) {
  let years = await getYears()
  years = years.filter(item => Number.parseInt(item.year) <= Number.parseInt(settings.currentYear))
  console.log('Get Commission')
  let commissionResult = await search('nodeTypeAlias:Commissions', true)
  if (!commissionResult.success) {
    res.status(500).json({
      error: 'unable to get Commission'
    })
  }
  let commissionArchive = []
  for (let yearId in years) {
    let year = years[yearId]
    let commissionForYear = commissionResult.data.find(item => item.Fields.parentID === year.id)
    if (commissionForYear) {
      commissionArchive.push({
        year: year.year,
        content: commissionForYear.Fields.title,
        htmlContent: commissionForYear.Fields.__Raw_htmlcontent || commissionForYear.Fields.htmlcontent
      })
    }
  }
  res.json(commissionArchive)
}

export {initializeCommission}
