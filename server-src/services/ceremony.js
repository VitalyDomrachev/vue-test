import { search } from '../utils/umbracoService'
import { getYears } from './years'
import settings from '../settings'

function initializeCeremony (app) {
  app.get('/getCeremony', getCeremony)
}

async function getCeremony (req, res) {
  let years = await getYears()
  years = years.filter(item => Number.parseInt(item.year) <= Number.parseInt(settings.currentYear))
  console.log('Get Ceremony')
  let ceremonyResult = await search('nodeTypeAlias:Awards', true)
  if (!ceremonyResult.success) {
    res.status(500).json({
      error: 'unable to get Ceremony'
    })
  }
  let ceremonyArchive = []
  for (let yearId in years) {
    let year = years[yearId]
    let ceremonyForYear = ceremonyResult.data.find(item => item.Fields.parentID === year.id)
    if (ceremonyForYear) {
      ceremonyArchive.push({
        year: year.year,
        content: ceremonyForYear.Fields.title,
        htmlContent: ceremonyForYear.Fields.__Raw_htmlcontent || ceremonyForYear.Fields.htmlcontent
      })
    }
  }
  res.json(ceremonyArchive)
}

export {initializeCeremony}
