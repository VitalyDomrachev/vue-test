import { search } from '../utils/umbracoService'
import { getYears } from './years'

function initializeCompetition (app) {
  app.get('/getCompetition', getCompetition)
}

async function getCompetition (req, res) {
  let years = await getYears()
  let currentYear = (new Date()).getFullYear()
  let currentYearId = years.find(item => Number.parseInt(item.year) === currentYear).id
  console.log('Get Competition')
  let competitionResult = await search('nodeTypeAlias:FoundersAdditionalNominations AND parentID:' + currentYearId, true)
  if (!competitionResult.success) {
    res.status(500).json({
      error: 'unable to get Competition'
    })
  }
  if (competitionResult.data && competitionResult.data.length && competitionResult.data.length === 1) {
    res.json({
      htmlContent: competitionResult.data[0].Fields.__Raw_htmlcontent || competitionResult.data[0].Fields.htmlcontent,
      title: competitionResult.data[0].Fields.title
    })
  } else {
    res.status(404).json({
      error: 'unable to get Competition. No data'
    })
  }
}

export {initializeCompetition}
