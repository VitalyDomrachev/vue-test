import { search } from '../utils/umbracoService'
import { getYears } from './years'
import settings from '../settings'

function initializeDocuments (app) {
  app.get('/getDocuments', getDocuments)
}

async function getDocuments (req, res) {
  let years = await getYears()
  years = years.filter(item => Number.parseInt(item.year) <= Number.parseInt(settings.currentYear))
  console.log('Get documents')
  let documentsResult = await search('nodeTypeAlias:Documents', true)
  if (!documentsResult.success) {
    res.status(500).json({
      error: 'unable to get Documents'
    })
  }
  let documentsArchive = []
  for (let yearId in years) {
    let year = years[yearId]
    let documentForYear = documentsResult.data.find(item => item.Fields.parentID === year.id)
    if (documentForYear) {
      documentsArchive.push({
        year: year.year,
        content: documentForYear.Fields.title,
        htmlContent: documentForYear.Fields.__Raw_htmlcontent || documentForYear.Fields.htmlcontent
      })
    }
  }
  res.json(documentsArchive)
}

export {initializeDocuments}
