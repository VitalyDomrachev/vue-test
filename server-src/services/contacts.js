import { search } from '../utils/umbracoService'
import { getYears } from './years'

function initializeContacts (app) {
  app.get('/getContacts', getContacts)
}

async function getContacts (req, res) {
  let years = await getYears()
  let currentYear = (new Date()).getFullYear()
  let currentYearId = years.find(item => Number.parseInt(item.year) === currentYear).id
  console.log('Get Contacts')
  let contactsResult = await search('nodeTypeAlias:ContactInformation AND parentID:' + currentYearId, true)
  if (!contactsResult.success) {
    res.status(500).json({
      error: 'unable to get Contacts'
    })
  }
  if (contactsResult.data && contactsResult.data.length && contactsResult.data.length === 1) {
    res.json({
      htmlContent: contactsResult.data[0].Fields.__Raw_htmlcontent || contactsResult.data[0].Fields.htmlcontent,
      title: contactsResult.data[0].Fields.title
    })
  } else {
    res.status(404).json({
      error: 'unable to get Contacts. No data'
    })
  }
}

export {initializeContacts}
