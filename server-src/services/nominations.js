import { search } from '../utils/umbracoService'
import { getYears } from './years'

function initializeNominations (app) {
  app.get('/getNominations', getNominations)
}

async function getNominations (req, res) {
  console.log('Get nominations')

  let results = await Promise.all([search('nodeTypeAlias:Nominations', true), search('nodeTypeAlias:NominationType', true), search('nodeTypeAlias:Nomination', true)])
  if (!results[0].success || !results[1].success) {
    res.status(500).json({
      error: 'unable to get Nominations'
    })
  }
  let archiveNominationsRaw = results[0].data
  let nominationTypesRaw = results[1].data
  let nominationsRaw = results[2].data
  let nominationTypes = {}
  for (let id in nominationTypesRaw) {
    let parentId = nominationTypesRaw[id].Fields.parentID
    if (!nominationTypes[parentId]) {
      nominationTypes[parentId] = []
    }
    nominationTypes[parentId].push(nominationTypesRaw[id])
  }
  let groupedNominations = {}
  for (let id in nominationsRaw) {
    let parentId = nominationsRaw[id].Fields.parentID
    if (!groupedNominations[parentId]) {
      groupedNominations[parentId] = []
    }
    groupedNominations[parentId].push(nominationsRaw[id])
  }
  let nominationsObj = {}

  for (let key in nominationTypes) {
    let nominations = {
      mainNominations: {},
      additionalNominations: {},
      sectorNominations: {},
      otherNominations: []
    }
    for (let key2 in nominationTypes[key]) {
      let newArr = [{nominations: []}]
      let group = nominationTypes[key][key2]
      let nominationsGroup = groupedNominations[group.Id]
      let parseArray = function (str) {
        return JSON.parse(str).map(item => {
          return {id: item.key, name: item.label}
        })
      }
      for (let i in nominationsGroup) {
        let itemIndex = !nominationsGroup[i].Fields.ExecutiveCompanyName
          ? 0
          : newArr.findIndex(item => item.executiveName === nominationsGroup[i].Fields.ExecutiveCompanyName)
        let nominationObj = {
          fullName: nominationsGroup[i].Fields.FullName,
          nomination: nominationsGroup[i].Fields.NominationName,
          range: nominationsGroup[i].Fields.RangeName,
          id: nominationsGroup[i].Id,
          hasFootnote: nominationsGroup[i].Fields.HasFootnote !== '0',
          sector: nominationsGroup[i].Fields.Sector,
          winnersFirstPlace: nominationsGroup[i].Fields.WinnersFirstPlace
            ? parseArray(nominationsGroup[i].Fields.WinnersFirstPlace)
            : undefined,
          winnersSecondPlace: nominationsGroup[i].Fields.WinnersSecondPlace
            ? parseArray(nominationsGroup[i].Fields.WinnersSecondPlace)
            : undefined,
          winnersThirdPlace: nominationsGroup[i].Fields.WinnersThirdPlace
            ? parseArray(nominationsGroup[i].Fields.WinnersThirdPlace)
            : undefined,
          shortList: nominationsGroup[i].Fields.ShortList
            ? parseArray(nominationsGroup[i].Fields.ShortList)
            : undefined
        }
        if (itemIndex >= 0) {
          newArr[itemIndex].nominations.push(nominationObj)
        } else {
          newArr.push({
            executiveName: nominationsGroup[i].Fields.ExecutiveCompanyName,
            nominations: [nominationObj]
          })
        }
      }
      if (newArr[0].nominations.length === 0) {
        newArr.shift()
      }
      if (group.Fields.IsMain === '1') {
        nominations.mainNominations.items = newArr
        nominations.mainNominations.title = group.Fields.NominationTypeTitle
        nominations.mainNominations.name = group.Fields.nodeName
        nominations.mainNominations.footnote = group.Fields.Footnote
      } else if (group.Fields.IsAdditional === '1') {
        nominations.additionalNominations.items = newArr
        nominations.additionalNominations.title = group.Fields.NominationTypeTitle
        nominations.additionalNominations.name = group.Fields.nodeName
        nominations.additionalNominations.footnote = group.Fields.Footnote
      } else if (group.Fields.IsSector === '1') {
        nominations.sectorNominations.items = newArr
        nominations.sectorNominations.title = group.Fields.NominationTypeTitle
        nominations.sectorNominations.name = group.Fields.nodeName
        nominations.sectorNominations.footnote = group.Fields.Footnote
      } else {
        nominations.otherNominations.push({
          items: newArr,
          title: group.Fields.NominationTypeTitle,
          name: group.Fields.nodeName,
          footnote: group.Fields.Footnote
        })
      }
    }
    nominationsObj[key] = nominations
  }
  let nominationsRes = {}
  let years = await getYears()
  for (let yearId in years) {
    let year = years[yearId]
    let nominationForYear = archiveNominationsRaw.find(item => item.Fields.parentID === year.id)
    if (nominationForYear) {
      nominationsRes[year.year] = {
        title: nominationForYear.Fields.NominationsTitle,
        nominations: nominationsObj[nominationForYear.Id]
      }
    }
  }
  res.json(nominationsRes)
}

export {initializeNominations}
