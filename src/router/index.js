import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '@/vue/pages/Main'
import About from '@/vue/pages/About'
import NotFound from '@/vue/pages/404'
import Competition from '@/vue/pages/Competition'
import CompositionCommission from '@/vue/pages/CompositionCommission'
import Ceremony from '@/vue/pages/Ceremony'
import Jury from '@/vue/pages/Jury'
import Contacts from '@/vue/pages/Contacts'
import Documents from '@/vue/pages/Documents'
import Nominations from '@/vue/pages/Nominations'
import Request from '@/vue/pages/Request'
import ShortList from '@/vue/pages/ShortList'
import Winners from '@/vue/pages/Winners'
import {setSelectedMenu, hideMenuItem} from '@/js/services/menuService'
import {getNominations, getYears} from '@/js/services/nominationsService'
import {setMaxYear, setYears, setSelectedYear, setIsReportAvailable} from '@/js/services/yearService'
import {getGeneralInfo} from '@/js/services/staticContentService'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Main',
      meta: { selectedMenuId: 0, title: 'Конкурс Годовых Отчетов' },
      props: (route) => ({ type: route.query.view, page: route.query.page, year: route.query.year, category: route.query.category, categoryId: route.query.category_id, fullPath: route.fullPath }),
      component: Main
    },
    {
      path: '/about',
      name: 'About',
      component: About,
      meta: { selectedMenuId: 1, selectedSubmenuId: 0, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/request',
      name: 'Request',
      component: Request,
      meta: { selectedMenuId: 1, selectedSubmenuId: 1, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/nominations',
      name: 'Nominations',
      component: Nominations,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 1, selectedSubmenuId: 2, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/documents',
      name: 'Documents',
      component: Documents,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 1, selectedSubmenuId: 3, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/competition',
      name: 'Competition',
      component: Competition,
      meta: { selectedMenuId: 1, selectedSubmenuId: 4, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/jury',
      name: 'Jury',
      component: Jury,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 1, selectedSubmenuId: 5, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/about/commission',
      name: 'Commission',
      component: CompositionCommission,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 1, selectedSubmenuId: 6, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/winners',
      name: 'Winners',
      component: Winners,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 2, selectedSubmenuId: 0, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/winners/short-list',
      name: 'ShortList',
      component: ShortList,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 2, selectedSubmenuId: 1, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/winners/ceremony',
      name: 'Ceremony',
      component: Ceremony,
      props: (route) => ({ year: route.query.year, path: route.path }),
      meta: { selectedMenuId: 2, selectedSubmenuId: 2, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/contacts',
      name: 'Contacts',
      component: Contacts,
      meta: { selectedMenuId: 3, title: 'Конкурс Годовых Отчетов' }
    },
    {
      path: '/*',
      name: '404',
      component: NotFound
    }
  ]
})

router.beforeEach((to, from, next) => {
  let menuId, submenuId
  let selectedMenuRoute = to.matched.find(record => record.meta.selectedMenuId || record.meta.selectedMenuId === 0)
  if (selectedMenuRoute) {
    menuId = selectedMenuRoute.meta.selectedMenuId
  }
  let selectedSubmenuRoute = to.matched.find(record => record.meta.selectedSubmenuId || record.meta.selectedSubmenuId === 0)
  if (selectedSubmenuRoute) {
    submenuId = selectedSubmenuRoute.meta.selectedSubmenuId
  }
  setSelectedMenu(menuId, submenuId)
  document.title = to.meta.title
  Promise.all([getNominations(), getGeneralInfo()]).then(result => {
    let nominations = result[0]
    setMaxYear(result[1].contestYear)
    setSelectedYear(to.query.year || result[1].contestYear)
    setYears(getYears(nominations))
    setIsReportAvailable(result[1].isReportFormAvailable)
    to.query.year = to.query.year || result[1].contestYear
    if (!result[1].isReportFormAvailable) {
      hideMenuItem(1, 1)
    }
    if (!result[1].isReportFormAvailable && to.name === 'Request') {
      next({replace: false, name: '404'})
    }
    next()
  })
})
export default router
