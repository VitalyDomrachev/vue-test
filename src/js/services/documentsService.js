import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function getDocuments (year) {
  return axios.get('/getDocuments', {headers: {'year': year}}).then(onSuccess, onError)
}

export {
  getDocuments
}
