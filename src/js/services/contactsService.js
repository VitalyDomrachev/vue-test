import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function getContacts (year) {
  return axios.get('/getContacts').then(onSuccess, onError)
}

export {
  getContacts
}
