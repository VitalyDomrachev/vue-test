import axios from 'axios'

function onSuccess (response) {
  return response.data
}
function processGeneralInfo (response) {
  let data = response.data
  let today = new Date()
  let isReportFormAvailable = new Date(data.startDate) < today && new Date(data.endDate) > today
  return {
    isReportFormAvailable: isReportFormAvailable,
    contestYear: data.year
  }
}

function onError (error) {
  console.log(error)
}

function getGeneralInfo () {
  return axios.get('/getGeneralInfo').then(processGeneralInfo, onError)
}
function getMainSlidesContent () {
  return axios.get('/getMainSlidesContent').then(onSuccess, onError)
}
function getAboutContent () {
  return axios.get('/getAboutContent').then(onSuccess, onError)
}
function getContactsContent () {
  return axios.get('/getContactsContent').then(onSuccess, onError)
}

export {
  getAboutContent, getContactsContent, getMainSlidesContent, getGeneralInfo
}
