import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function getCompetition (year) {
  return axios.get('/getCompetition').then(onSuccess, onError)
}

export {
  getCompetition
}
