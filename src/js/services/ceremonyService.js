import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.error(error)
}

function getCeremony (year) {
  return axios.get('/getCeremony', {headers: {'year': year}}).then(onSuccess, onError)
}

export {
  getCeremony
}
