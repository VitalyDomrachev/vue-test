import axios from 'axios'

let nominationsCache
let winnersCache

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function concatNominations (nomination) {
  return [].concat(...(nomination.items || []).map(group => group.nominations.map(item => {
    return {name: item.fullName, id: item.id}
  })))
}

function getNominations () {
  if (nominationsCache) {
    return new Promise(function (resolve, reject) {
      resolve(nominationsCache)
    })
  }
  return axios.get('/getNominations', {headers: {'year': 2018}}).then(onSuccess, onError).then(value => {
    nominationsCache = value
    return nominationsCache
  })
}

function getSectors (nominations) {
  let sectors = [].concat(...(nominations.sectorNominations.items || []).map(group => group.nominations.map(item => {
    return {name: item.sector, id: item.id}
  })))
  sectors.push({name: 'Иное', id: 0})
  return sectors
}
function getYears (nominations) {
  return Object.keys(nominations)
    .map(function (item) { return Number.parseInt(item) })
    .filter(function (item) { return Number.isInteger(item) })
    .sort((a, b) => b - a)
}

function getWinners () {
  if (winnersCache) {
    return new Promise(function (resolve, reject) {
      resolve(winnersCache)
    })
  }
  return getNominations().then(result => {
    let winners = {}
    let years = getYears(result)
    for (let yearId in years) {
      let year = years[yearId]
      let nominations = nominationsCache[year].nominations
      let nominationsGroups = []
      if (nominations.mainNominations && nominations.mainNominations.items) nominationsGroups = nominationsGroups.concat(...nominations.mainNominations.items)
      if (nominations.additionalNominations && nominations.additionalNominations.items) nominationsGroups = nominationsGroups.concat(...nominations.additionalNominations.items)
      if (nominations.sectorNominations && nominations.sectorNominations.items) nominationsGroups = nominationsGroups.concat(...nominations.sectorNominations.items)
      for (let nominationGroupId in nominations.otherNominations) {
        nominationsGroups.concat(...nominations.otherNominations[nominationGroupId].items)
      }
      let nominationsFlat = []
      for (let nominationId in nominationsGroups) {
        nominationsFlat = nominationsFlat.concat(...nominationsGroups[nominationId].nominations)
      }
      for (let nominationId in nominationsFlat) {
        let nomination = nominationsFlat[nominationId]
        for (let winnerId in nomination.winnersFirstPlace) {
          let winner = nomination.winnersFirstPlace[winnerId]
          if (winners[winner.id] === undefined) {
            winners[winner.id] = { gold: [], silver: [], bronze: [], shortList: [] }
          }
          winners[winner.id].gold.push(year)
        }
        for (let winnerId in nomination.winnersSecondPlace) {
          let winner = nomination.winnersSecondPlace[winnerId]
          if (winners[winner.id] === undefined) {
            winners[winner.id] = { gold: [], silver: [], bronze: [], shortList: [] }
          }
          winners[winner.id].silver.push(year)
        }
        for (let winnerId in nomination.winnersThirdPlace) {
          let winner = nomination.winnersThirdPlace[winnerId]
          if (winners[winner.id] === undefined) {
            winners[winner.id] = { gold: [], silver: [], bronze: [], shortList: [] }
          }
          winners[winner.id].bronze.push(year)
        }
        for (let winnerId in nomination.shortList) {
          let winner = nomination.shortList[winnerId]
          if (winners[winner.id] === undefined) {
            winners[winner.id] = { gold: [], silver: [], bronze: [], shortList: [] }
          }
          winners[winner.id].shortList.push(year)
        }
      }
    }
    winnersCache = winners
    return winnersCache
  })
}

export {
  getNominations, concatNominations, getSectors, getWinners, getYears
}
