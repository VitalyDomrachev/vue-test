import URI from 'urijs'

function getCurrentUrl (vm) {
  return getUrl(vm.$root.$route.fullPath)
}

function getUrl (urlString) {
  let uri = new URI(urlString)
  return uri
}

function appendParam (uri, name, value) {
  uri.removeQuery(name)
  uri.addQuery(name, value)
}

function removeParam (uri, name) {
  uri.removeQuery(name)
}

function getParam (uri, name) {
  let searchParams = uri.query(true)
  return searchParams[name]
}

export {getCurrentUrl, appendParam, getParam, removeParam, getUrl}
