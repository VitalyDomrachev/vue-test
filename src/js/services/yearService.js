let maxYear
let years = []
let selectedYear
let isReportAvailable = false

function getCurrentYear () {
  return (new Date()).getFullYear()
}

function getMaxYear () {
  return maxYear
}

function setMaxYear (value) {
  maxYear = value
}
function getSelectedYear () {
  return selectedYear
}

function setSelectedYear (value) {
  selectedYear = value
}

function setYears (value) {
  years = value
}

function getYears () {
  return years
}

function setIsReportAvailable (value) {
  isReportAvailable = value
}

function getIsReportAvailable () {
  return isReportAvailable
}

function getFilteredYears () {
  return getYears().filter(item => item <= getMaxYear())
}

function romanize (year) {
  if (!+year) { return NaN }
  let num = year - 1997
  let digits = String(+num).split('')
  let key = ['', 'C', 'CC', 'CCC', 'CD', 'D', 'DC', 'DCC', 'DCCC', 'CM',
    '', 'X', 'XX', 'XXX', 'XL', 'L', 'LX', 'LXX', 'LXXX', 'XC',
    '', 'I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX']
  let roman = ''
  let i = 3
  while (i--) { roman = (key[+digits.pop() + (i * 10)] || '') + roman }
  return Array(+digits.join('') + 1).join('M') + roman
}

export {
  getCurrentYear,
  setMaxYear,
  getYears,
  setYears,
  getMaxYear,
  getSelectedYear,
  setSelectedYear,
  romanize,
  getFilteredYears,
  setIsReportAvailable,
  getIsReportAvailable
}
