import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function getAbout (year) {
  return axios.get('/getAbout').then(onSuccess, onError)
}

export {
  getAbout
}
