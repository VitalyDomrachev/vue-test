import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.error(error)
}

function getCommission (year) {
  return axios.get('/getCommission', {headers: {'year': year}}).then(onSuccess, onError)
}

export {
  getCommission
}
