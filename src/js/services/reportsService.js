import axios from 'axios'
import {getUrl, appendParam} from '@/js/services/urlService'

let reportsCache = {}

async function getReports (year) {
  if (reportsCache[year]) return reportsCache[year]

  let uri = getUrl('/getReports')
  appendParam(uri, 'year', year)
  try {
    let result = await axios.get(uri.href())
    reportsCache[year] = result.data
    return result.data
  } catch (err) { console.log(err) }
}

async function addReport (report, captcha) {
  try {
    let result = await axios.post('/addReport', {report: report, captcha: captcha})
    return result.data
  } catch (err) { console.log(err) }
}

export {
  getReports, addReport
}
