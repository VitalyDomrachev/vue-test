const menuItems = [
  { name: 'ГЛАВНАЯ', view: 'Main', subMenu: [], selected: false },
  {
    name: 'О КОНКУРСЕ',
    view: 'About',
    subMenu: [
      { name: 'о конкурсе', view: 'About', selected: false },
      { name: 'заявка на участие', view: 'Request', selected: false },
      { name: 'номинации конкурса', view: 'Nominations', selected: false },
      { name: 'документы конкурса', view: 'Documents', selected: false },
      { name: 'учредителям дополнительных номинаций', view: 'Competition', selected: false },
      { name: 'состав жюри', view: 'Jury', selected: false },
      { name: 'состав конкурсной комиссии', view: 'Commission', selected: false }
    ],
    selected: false
  },
  {
    name: 'ПОБЕДИТЕЛИ',
    view: 'Winners',
    subMenu: [
      { name: 'победители конкурса', view: 'Winners', selected: false },
      { name: 'шорт-листы номинаций', view: 'ShortList', selected: false },
      { name: 'церемония награждения', view: 'Ceremony', selected: false }
    ],
    selected: false
  },
  { name: 'КОНТАКТЫ', view: 'Contacts', subMenu: [], selected: false }
]

var selectedMenu = 0
var selectedSubmenu = 0

function getSelectedMenuId () {
  return selectedMenu
}

function getSelectedSubmenuId () {
  return selectedSubmenu
}

function getSubmenu () {
  return menuItems[getSelectedMenuId()].subMenu
}

function getMenu () {
  return menuItems
}

function setSelectedMenu (menuId, submenuId) {
  if (menuId !== undefined) {
    if (selectedMenu !== undefined) menuItems[selectedMenu].selected = false
    if (selectedMenu !== undefined &&
      selectedSubmenu !== undefined &&
      menuItems[selectedMenu].subMenu.length > selectedSubmenu) {
      menuItems[selectedMenu].subMenu[selectedSubmenu].selected = false
    }
    menuItems[menuId].selected = true
    if (submenuId !== undefined) menuItems[menuId].subMenu[submenuId].selected = true
    selectedMenu = menuId
    selectedSubmenu = submenuId
  }
}

function hideMenuItem (menuId, submenuId) {
  if (menuId !== undefined && submenuId !== undefined) menuItems[menuId].subMenu[submenuId].hidden = true
}

export {setSelectedMenu, getSubmenu, getMenu, getSelectedMenuId, getSelectedSubmenuId, hideMenuItem}
