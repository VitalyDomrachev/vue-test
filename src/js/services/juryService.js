import axios from 'axios'

function onSuccess (response) {
  return response.data
}

function onError (error) {
  console.log(error)
}

function getJury (year) {
  return axios.get('/getJury', {headers: {'year': year}}).then(onSuccess, onError)
}

export {
  getJury
}
