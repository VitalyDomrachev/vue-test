import Categories from '@/vue/components/reports/Categories.vue'
import Img from '@/vue/components/Img.vue'
import Documents from '@/vue/components/Documents.vue'
import Rewards from '@/vue/components/Rewards.vue'
import {getReports} from '@/js/services/reportsService.js'
import {getWinners} from '@/js/services/nominationsService.js'
import {getCurrentUrl, appendParam, removeParam} from '@/js/services/urlService'
import $ from 'jquery'
import 'jquery-clickout'
import scrollToElement from 'scroll-to-element'
import ModalReport from '@/vue/components/modals/ModalReport.vue'
import Loading from '@/vue/components/Loading.vue'

export default {
  data () {
    return {
      title: 'Отчёты',
      views: [ 'tiles', '' ],
      numberOfPages: 0,
      reportsPerPage: 0,
      mobileCategoriesVisible: false,
      reports: [],
      allReports: [],
      detailsLabel: 'подробнее',
      selectedReport: undefined,
      selectedPage: 1,
      filter: {},
      dataLoading: true
    }
  },
  components: {
    Img,
    Documents,
    Rewards,
    Categories,
    ModalReport,
    Loading
  },
  props: {
    type: {
      type: String,
      required: false,
      default: 'nominations'
    },
    fullPath: {
      type: String,
      required: false,
      default: '/'
    },
    page: {
      type: String,
      required: false,
      default: '1'
    },
    category: {
      type: String,
      required: false,
      default: ''
    },
    categoryId: {
      type: String,
      required: false,
      default: ''
    },
    year: {
      type: String,
      required: false
    }
  },
  created () {
    this.refreshReports()
  },
  watch: {
    fullPath: function (newVal, oldVal) {
      this.refreshReports()
    },
    page: function (newVal, oldVal) {
      scrollToElement('.reports__heading', {duration: 500})
      this.selectedPage = newVal
    }
  },
  methods: {
    selectReport (report) {
      this.selectedReport = report
    },
    refreshReports () {
      let vm = this
      if (this.year && this.filter.year === this.year) {
        this.filterReports()
      } else {
        this.filter.year = this.year
        vm.dataLoading = true
        getReports(this.year).then(result => {
          vm.dataLoading = false
          if (result) {
            vm.allReports = result
            vm.updateWinners()
            vm.filterReports()
          } else {
            result = []
          }
        })
      }
    },
    updateWinners () {
      let vm = this
      vm.dataLoading = true
      getWinners().then(winners => {
        for (let reportId in vm.allReports) {
          let report = vm.allReports[reportId]
          report.rewards = []
          if (winners[report.company.id]) {
            let rewards = winners[report.company.id]
            for (let rewardId in rewards.gold) {
              report.rewards.push({medal: '_gold', year: rewards.gold[rewardId]})
            }
            for (let rewardId in rewards.silver) {
              report.rewards.push({medal: '_silver', year: rewards.silver[rewardId]})
            }
            for (let rewardId in rewards.bronze) {
              report.rewards.push({medal: '_bronze', year: rewards.bronze[rewardId]})
            }
          }
        }
        vm.dataLoading = false
      })
    },
    filterReports () {
      let filteredReports = this.allReports
      if (this.category && this.categoryId) {
        let vm = this
        if (this.category === 'nomination' || this.category === 'sector') {
          filteredReports = filteredReports.filter(
            item => item.nominations.find(nomination => nomination === vm.categoryId))
        } else if (this.category === 'name') {
          filteredReports = filteredReports.filter(item => item.company.id === vm.categoryId)
        }
      }

      this.reportsPerPage = this.type === 'tiles' ? 24 : 12
      this.numberOfPages = Math.ceil(filteredReports.length / this.reportsPerPage)
      this.selectedPage = this.page ? this.page : 1
      this.selectedPage = this.selectedPage > this.numberOfPages ? this.numberOfPages : this.selectedPage
      this.reports = filteredReports.slice(this.reportsPerPage * (this.selectedPage - 1), this.reportsPerPage * this.selectedPage)
    },
    mobileCategoriesClick (event) {
      if (!this.mobileCategoriesVisible) {
        this.mobileCategoriesVisible = true
        let $item = $(this.$refs.categoriesBlock)
        $item.on('clickout', () => {
          this.mobileCategoriesVisible = false
          $item.off('clickout')
        })
        event.stopPropagation()
      }
    },
    getPageLink (index) {
      let uri = getCurrentUrl(this)
      appendParam(uri, 'page', index)
      return uri.href()
    },
    getViewLink (view) {
      let uri = getCurrentUrl(this)
      if (view) {
        appendParam(uri, 'view', view)
        removeParam(uri, 'page')
      } else {
        removeParam(uri, 'view')
        removeParam(uri, 'page')
      }
      return uri.href()
    }
  }
}
