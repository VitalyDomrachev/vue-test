import $ from 'jquery'
import 'jquery-clickout'
import {getNominations, concatNominations, getSectors} from '@/js/services/nominationsService'
import {getFilteredYears} from '@/js/services/yearService'
import {getReports} from '@/js/services/reportsService'
import {getCurrentUrl, appendParam, removeParam} from '@/js/services/urlService'
import Loading from '@/vue/components/Loading.vue'

export default {
  data () {
    return {
      categories: {
        nomination: {
          name: 'по номинации',
          categoriesBlocks: []},
        name: { name: 'по наименованию', categoriesBlocks: [{ mods: '_name', id: 'name' }] },
        sector: { name: 'по отрасли', categoriesBlocks: [{ id: 'sector' }] },
        yearCategory: { name: 'по году', items: [] }
      },
      selectedCategory: '',
      categoriesBlockVisible: -1,
      dictionariesForFilter: {},
      selectedYear: '',
      dataLoading: true
    }
  },
  created () {
    this.getDictionariesForFilter()
  },
  props: {
    year: {
      type: String,
      required: true
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      this.selectedYear = newVal
      if (this.selectedCategory === 'yearCategory') {
        this.hideCategory()
      }
    },
    selectedYear: function (newVal, oldVal) {
      this.updateFilter(newVal)
    }
  },
  methods: {
    updateCompanyList (year) {
      let vm = this
      this.dataLoading = true
      getReports(year).then(result => {
        vm.companies = result.map(item => item.company)
        this.categories.name.categoriesBlocks[0].items = result.map(item => item.company).sort((a, b) => {
          if (a.name > b.name) {
            return 1
          } else if (a.name < b.name) {
            return -1
          } else { return 0 }
        })
        vm.dataLoading = false
      })
    },
    getDictionariesForFilter: function () {
      this.dataLoading = true
      getNominations().then(results => {
        let nominations = results
        let years = getFilteredYears()
        this.dictionariesForFilter = {}
        for (let yearId in years) {
          let year = years[yearId]
          this.dictionariesForFilter[year] = nominations[year].nominations
          this.categories.yearCategory.items = years
          this.categories.name.categoriesBlocks[0].items = this.companies
        }
        this.selectedYear = this.year
        this.updateFilter(this.selectedYear)
        this.dataLoading = false
      })
    },
    updateFilter: function (year) {
      this.updateCompanyList(year)
      this.updateFilterViewModel(this.dictionariesForFilter[year])
    },
    updateFilterViewModel: function (nominations) {
      this.categories.nomination.categoriesBlocks.length = 0
      if (nominations.mainNominations) {
        this.categories.nomination.categoriesBlocks.push({
          items: concatNominations(nominations.mainNominations), title: nominations.mainNominations.name, id: 'nomination', mobileToggle: true
        })
      }
      if (nominations.additionalNominations) {
        this.categories.nomination.categoriesBlocks.push({
          items: concatNominations(nominations.additionalNominations), title: nominations.additionalNominations.name, id: 'nomination', mobileToggle: true
        })
      }
      if (nominations.sectorNominations) {
        this.categories.nomination.categoriesBlocks.push({
          items: concatNominations(nominations.sectorNominations), title: nominations.sectorNominations.name, id: 'nomination', mobileToggle: true
        })
      }
      for (let i in nominations.otherNominations) {
        this.categories.nomination.categoriesBlocks.push({
          title: nominations.otherNominations[i].name,
          items: concatNominations(nominations.otherNominations[i]),
          id: 'other_nominations',
          mobileToggle: true
        })
      }

      let sectors = getSectors(nominations)
      this.categories.sector.categoriesBlocks[0].items = sectors
    },
    getYearLink: function (item) {
      let url = getCurrentUrl(this)
      removeParam(url, 'category')
      removeParam(url, 'category_id')
      appendParam(url, 'year', item)
      removeParam(url, 'page')
      return url.href()
    },
    getCategoryLink: function (name, value) {
      let url = getCurrentUrl(this)
      appendParam(url, 'category', name)
      appendParam(url, 'category_id', value.id)
      appendParam(url, 'year', this.selectedYear)
      removeParam(url, 'page')
      return url.href()
    },
    categorySelected: function (event) {
      this.hideCategory()
      this.hideCategoryBlock()
    },
    yearSelected: function (item) {
      this.selectedYear = item
    },
    hideCategoryBlock () {
      if (this.categoriesBlockVisible > 0) {
        let $item = $(this.$el).find(`.categories__dropdown[dataid='${this.categoriesBlockVisible}']`)
        $item.off('clickout')
        this.categoriesBlockVisible = -1
      }
    },
    hideCategory () {
      if (this.selectedCategory) {
        let $item = $(this.$el).find(`.categories__dropdown[dataid='${this.selectedCategory}']`)
        $item.off('clickout')
        this.selectedCategory = -1
        this.selectedYear = this.year
      }
    },
    categoriesBlockClick (event) {
      let id = parseInt(event.target.dataset.id)
      this.hideCategoryBlock()
      if (this.categoriesBlockVisible < 0 || this.categoriesBlockVisible !== id) {
        this.categoriesBlockVisible = id
        let $item = $(this.$el).find(`.categories-block__list[dataid='${id}']`)
        $item.on('clickout', () => {
          this.hideCategoryBlock()
        })
        event.stopPropagation()
      }
    },
    categoryClick (event) {
      let selectedCategory = event.target.dataset.id
      this.hideCategory()
      if (this.selectedCategory !== selectedCategory) {
        this.selectedCategory = selectedCategory
        let $item = $(this.$el).find(`.categories__dropdown[dataid='${selectedCategory}']`)
        $item.on('clickout', () => {
          this.hideCategory()
        })
        event.stopPropagation()
      }
    }
  },
  components: {
    Loading
  }
}
