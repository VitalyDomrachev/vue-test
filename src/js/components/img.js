export default {
  computed: {
    srcset () {
      return this.url || ''
    },
    src () {
      return this.url || ''
    }
  },
  props: {
    url: {
      type: String,
      required: false,
      default: ''
    }
  }
}
