import ModalLogin from '@/vue/components/modals/ModalLogin.vue'
import ModalRegister from '@/vue/components/modals/ModalRegister.vue'

export default {
  data () {
    return {
      login: { link: '/', title: 'Войти' },
      register: { link: '/', title: 'Зарегистрироваться' }
    }
  },
  components: {
    ModalLogin,
    ModalRegister
  }
}
