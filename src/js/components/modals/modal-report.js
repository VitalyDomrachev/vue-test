import Slider from '@/vue/components/Slider'
import Rewards from '@/vue/components/Rewards'
import Documents from '@/vue/components/Documents'
import $ from 'jquery'

export default {
  props: {
    report: {
      type: Object,
      required: false
    }
  },
  mounted () {
    let reportModal = $(this.$el)
    let vm = this
    reportModal.on('shown.bs.modal', () => {
      vm.showModal()
    })
    reportModal.on('hidden.bs.modal', () => {
      vm.hideModal()
    })
  },
  methods: {
    hideModal () {
      this.$refs.slider.stop()
    },
    showModal () {
      this.$refs.slider.start()
    }
  },
  components: {
    Slider,
    Rewards,
    Documents
  }
}
