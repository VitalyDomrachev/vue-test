export default {
  data () {
    return {
      title: 'Войти',
      emailTitle: 'E-mail',
      passwordTitle: 'Пароль',
      forgotPasswordTitle: 'Забыли пароль?',
      forgotPasswordLink: '/',
      submitTitle: 'Войти'
    }
  }
}
