export default {
  data () {
    return {
      title: 'Регистрация',
      emailTitle: 'E-mail',
      passwordTitle: 'Пароль',
      confirmPassword: 'Подтвердите пароль',
      termsAndConditionsTitle: 'Создавая аккаунт, я принимаю условия пользования',
      termsAndConditionsSiteLink: 'javascript:void(0);',
      termsAndConditionsSiteTitle: 'веб-сайтом',
      termsAndConditionsPolicyLink: 'javascript:void(0);',
      termsAndConditionsPolicyTitle: 'политику конфиденциальности',
      submitTitle: 'Зарегистрироваться'
    }
  }
}
