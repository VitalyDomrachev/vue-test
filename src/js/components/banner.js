import $ from 'jquery'
import Submenu from '@/vue/components/Submenu.vue'

export default {
  data () {
    return {
      submenuBlockMarginLeft: 0
    }
  },
  props: {
    bannerUrl: {
      type: String,
      required: true
    }
  },
  methods: {
    updateSubmenuMargin () {
      if (this.$mainMenu) {
        let activeOffset = this.$mainMenu.filter('.active').offset() || {}
        let firstOffset = this.$mainMenu.first().offset() || {}
        let delta = activeOffset.left - firstOffset.left
        this.submenuBlockMarginLeft = +delta.toFixed(1)
      }
    }
  },
  mounted () {
    this.$mainMenu = $('.js-submenu-position-item')
    this.$nextTick(function () {
      window.addEventListener('resize', () => this.updateSubmenuMargin())
      this.updateSubmenuMargin()
    })
  },
  components: {
    Submenu
  }
}
