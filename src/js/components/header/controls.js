import $ from 'jquery'
import 'jquery-clickout'

export default {
  data () {
    return {
      searchLabel: 'Поиск',
      userLink: 'javascript:void(0);',
      searchLink: 'javascript:void(0);',
      searchFormVisible: false
    }
  },
  mounted () {
    this.$nextTick(function () {
      let $item = $(this.$refs.userPopover)
      $item.popover({
        html: true,
        content () {
          let $t = $item
          let $content = $($t.data('content-selector'))
          let content = ''

          if ($content.length) {
            content = $content.html()
          }

          return content
        }
      })

      $item.on('shown.bs.popover', () => {
        $item = $item.add('.popover')
        $item.on('clickout', () => {
          $item.popover('hide')
        })
      })

      $item.on('hide.bs.popover', () => $item.off('clickout'))

      $('.modal').on('show.bs.modal', () => {
        $item.each((i, item) => $(item).popover('hide'))
      })
    })
  },
  methods: {
    searchClick (event) {
      if (!this.searchFormVisible) {
        this.searchFormVisible = true
        let $item = $(this.$refs.searchForm)
        $item.on('clickout', () => {
          this.searchFormVisible = false
          $item.off('clickout')
        })
        event.stopPropagation()
      }
    }
  }
}
