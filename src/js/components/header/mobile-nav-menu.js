import {getMenu} from '@/js/services/menuService.js'
import $ from 'jquery'
import 'jquery-clickout'

export default {
  data () {
    return {
      menuItems: getMenu(),
      mobileSubmenuOpened: -1
    }
  },
  methods: {
    openMobileSubmenu (id) {
      if (this.mobileSubmenuOpened < 0 || this.mobileSubmenuOpened !== id) {
        this.mobileSubmenuOpened = id
        let vm = this
        let $item = $(this.$el).find(`.mobile-nav__submenu[dataid='${id}']`)
        $item.on('clickout', () => {
          if ($item.attr('dataid') == vm.mobileSubmenuOpened) {
            vm.mobileSubmenuOpened = -1
            $item.off('clickout')
          }
        })
        event.stopPropagation()
      }
    }
  }
}
