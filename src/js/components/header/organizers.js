import Img from '@/vue/components/Img.vue'

export default {
  data () {
    return {
      msg: 'Welcome to Organizers',
      title: 'ОРГАНИЗАТОРЫ',
      organizers: [
        {
          imgUrl: '/static/moex-25.png'
        },
        {
          imgUrl: '/static/rcb.png'
        }
      ]
    }
  },
  components: { Img }
}
