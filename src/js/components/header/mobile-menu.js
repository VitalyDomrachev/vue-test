import MobileNavAuth from '@/vue/components/header/MobileNavAuth.vue'
import MobileNavMenu from '@/vue/components/header/MobileNavMenu.vue'
import Organizers from '@/vue/components/header/Organizers.vue'

export default {
  data () {
    return {
      msg: 'Welcome to Mobile Menu',
      authAvailable: false
    }
  },
  components: {
    MobileNavMenu,
    MobileNavAuth,
    Organizers
  }
}
