import {getMenu} from '@/js/services/menuService.js'

export default {
  data () {
    return {
      menuItems: getMenu()
    }
  }
}
