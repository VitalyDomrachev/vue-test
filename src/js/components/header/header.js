import MobileMenu from '@/vue/components/header/MobileMenu.vue'
import HeaderMenu from '@/vue/components/header/HeaderMenu.vue'
import Organizers from '@/vue/components/header/Organizers.vue'
import Controls from '@/vue/components/header/Controls.vue'
import Img from '@/vue/components/Img.vue'
import $ from 'jquery'
import 'jquery-clickout'

export default {
  data () {
    return {
      msg: 'Welcome to Header',
      headerButtonLink: 'javascript:void(0);',
      headerLogoLink: 'javascript:void(0);',
      logoUrl: '/static/logo.png',
      mobileMenuVisible: false
    }
  },
  methods: {
    mobileMenuClick (event) {
      if (!this.mobileMenuVisible) {
        this.mobileMenuVisible = true
        let $item = $(this.$refs.mobileMenu)
        $item.on('clickout', () => {
          this.mobileMenuVisible = false
          $item.off('clickout')
        })
        event.stopPropagation()
      }
    }
  },
  components: {
    Organizers,
    HeaderMenu,
    MobileMenu,
    Controls,
    Img
  }
}
