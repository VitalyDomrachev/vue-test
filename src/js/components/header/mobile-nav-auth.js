export default {
  data () {
    return {
      menuItems: [
        {name: 'Войти', toggle: 'modal', target: '#login'},
        {name: 'Зарегистрироваться', toggle: 'modal', target: '#register'}
      ],
      withoutSubmenu: true
    }
  }
}
