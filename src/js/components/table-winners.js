export default {
  data () {
    return {
      nominationColumnTitle: 'Наименование номинации',
      winnersColumnTitle: 'Победители и призёры'
    }
  },
  props: {
    winners: {
      type: Array,
      required: true
    }
  }
}
