export default {
  data () {
    return {
      nominationColumnTitle: 'Наименование номинации',
      nominationFounderColumnTitle: 'Учредитель номинации'
    }
  },
  props: {
    nominations: {
      type: Array,
      required: false,
      default: () => []
    },
    titleHtml: {
      type: String,
      required: true
    }
  }
}
