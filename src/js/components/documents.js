export default {
  props: {
    documents: {
      type: Object,
      required: true
    }
  }
}
