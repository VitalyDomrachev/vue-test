import Header from '@/vue/components/header/Header.vue'
import Footer from '@/vue/components/Footer.vue'
import Modals from '@/vue/components/modals/Modals.vue'

export default {
  components: {
    Header,
    Footer,
    Modals
  }
}
