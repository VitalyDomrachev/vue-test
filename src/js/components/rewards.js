export default {
  props: {
    rewards: {
      type: Array,
      required: true
    }
  }
}
