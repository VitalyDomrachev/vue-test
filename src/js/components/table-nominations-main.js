export default {
  props: {
    nominations: {
      type: Array,
      required: false,
      default: () => []
    },
    titleHtml: {
      type: String,
      required: true
    }
  }
}
