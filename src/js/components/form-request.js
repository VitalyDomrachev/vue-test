import Img from '@/vue/components/Img.vue'
import Inputmask from 'inputmask'
import VueRecaptcha from 'vue-recaptcha'
import {addReport} from '@/js/services/reportsService'
import {initializeValidation, validate} from '@/js/helpers/validation'
import {getNominations, concatNominations, getSectors} from '@/js/services/nominationsService'
import {getCurrentYear} from '@/js/services/yearService'

export default {
  data () {
    return {
      submitterInformation: 'Информация о Заявителе',
      applicantFullNameTitle: 'Полное наименование',
      applicantShortNameTitle: 'Краткое наименование по Уставу (для отображения на сайте Конкурса)',
      postAddressTitle: 'Почтовый адрес',
      internetAddressTitle: 'Адрес сайта в сети Интернет',
      contactPersonTitle: 'Контактное лицо Заявителя',
      lastnameTitle: 'Фамилия',
      firstnameTitle: 'Имя',
      middlenameTitle: 'Отчество',
      positionTitle: 'Должность',
      emailTitle: 'E-mail',
      phoneTitle: 'Номер телефона (с междугородним кодом)',
      shortDescriptionTitle: 'Краткая информация о годовом отчете Заявителя',
      shortAboutCompanyTitle: 'Краткая информация о компании',
      shortAboutCompanyTitleLine2: '(Может содержать краткое описание компании)',
      sectorTitle: 'Основной вид/виды деятельности (отрасль)',
      otherSectorTitle: 'Введите иное название вида деятельности',
      helpSitesTitle: 'Наименование(я) и сайт(ы) компаний, оказавших Вам услуги по подготовке годового отчета',
      mainNominationsTitle: 'Основные номинации',
      additionalNominationsTitle: 'Дополнительные номинации',
      reportLinkTitle: 'Вставьте ссылку на годовой отчет',
      reportTitle: 'Прикрепить ссылку на годовой отчет',
      additionalNominations: [],
      mainNominations: [],
      reportCategories: [],
      reportCategoryNames: ['Годовой отчет на русском', 'Годовой отчет на английском', 'Интерактивный годовой отчет', 'Социальный отчет на русском', 'Социальный отчет на английском'],
      sectorNames: [],
      disclamerHtml: `<p>Настоящим Заявитель просит допустить к участию в XX Ежегодном конкурсе годовых отчетов.
      <p>С условиями Положения об организации XX Ежегодного конкурса годовых отчетов и объявленными условиями проведения Конкурса Заявитель ознакомлен и согласен.</p>
      <p>Настоящим Заявитель выражает свое согласие на участие в одной из основных номинациях Конкурса, которые указаны в настоящей анкете-заявлении, а также на участие в дополнительных номинациях Конкурса из числа указанных в настоящей анкете-заявлении.</p>
      <p>Настоящим Заявитель выражает свое согласие на публикацию направляемого для участия в Конкурсе годового отчета Заявителя за 2016 год на официальном сайте Конкурса, а также на предоставление доступа к такому годовому отчету всем заинтересованным лицам, независимо от целей его получения.</p>
      </p>`,
      agreements: [
        { title: 'Да, подтверждаю', checked: false },
        { title: 'Согласен на обработку персональных данных', checked: false }
      ],
      submitTitle: 'Оформить заявку',
      selectedSector: -1,
      otherSector: '',
      selectedAdditionalNominations: [],
      selectedMainNomination: -1,
      selectedReports: [],
      submitAvailable: false,
      captchaResponse: undefined,
      newReport: {
        contactPersonFirstName: '',
        contactPersonLastName: '',
        contactPersonMiddleName: '',
        contactPersonEmail: '',
        contactPersonPhone: '',
        contactPersonPosition: '',
        applicantFullName: '',
        applicantShortName: '',
        applicantPostAddress: '',
        applicantInternetAddress: '',
        shortReportDescription: '',
        shortCompanyDescription: '',
        helpSites: ''
        // contactPersonFirstName: 'FirstName',
        // contactPersonLastName: 'LastName',
        // contactPersonMiddleName: 'MiddleName',
        // contactPersonEmail: 'email@gmail.ru',
        // contactPersonPhone: '+7 (999) 999-99-98',
        // contactPersonPosition: 'Director',
        // applicantFullName: 'CompanyFullName',
        // applicantShortName: 'CompanyShortName',
        // applicantPostAddress: 'PostAddress',
        // applicantInternetAddress: 'CompanySite.ru',
        // shortReportDescription: 'Short Report Description',
        // shortCompanyDescription: 'Short Company Description',
        // helpSites: 'Who helped to prepare report, Sites'
      },
      nominations: []
    }
  },
  components: {
    Img,
    VueRecaptcha
  },
  created () {
    let self = this

    getNominations().then(result => {
      let currentYear = getCurrentYear()
      self.nominations = (result[currentYear] || {}).nominations
      if (self.nominations) {
        self.mainNominations = concatNominations(self.nominations.mainNominations)
        self.additionalNominations = concatNominations(self.nominations.additionalNominations)
        self.additionalNominations.push({name: 'Не участвую в дополнительных номинациях', id: 0})
        self.sectorNames = getSectors(self.nominations)
      }
    })
    this.reportCategories.length = 0
    for (const category in this.reportCategoryNames) {
      this.reportCategories.push({ title: this.reportCategoryNames[category], documentLink: '', id: category })
    }
  },
  mounted () {
    initializeValidation(this)
    // инициализация масок
    Inputmask({
      clearIncomplete: true
    }).mask('[data-inputmask]')
  },
  watch: {
    selectedAdditionalNominations: function (newVal, oldVal) {
      let other = 0
      if (newVal.length > 1) {
        var index = oldVal.indexOf(other)
        if (index !== -1) {
          newVal.splice(index, 1)
        } else if (newVal.find(item => item === other) !== undefined) {
          newVal.length = 0
          newVal.push(other)
        }
      }
    }
  },
  methods: {
    prepareNewReport: function () {
      let nominations = []
      if (this.selectedSector > 0) {
        nominations.push(this.selectedSector)
      }
      nominations.push(this.selectedMainNomination)
      if (!this.selectedAdditionalNominations.find(item => item === 0)) {
        nominations.push(...this.selectedAdditionalNominations)
      }
      let report = {
        FirstName: this.newReport.contactPersonFirstName,
        LastName: this.newReport.contactPersonLastName,
        MiddleName: this.newReport.contactPersonMiddleName,
        Email: this.newReport.contactPersonEmail,
        Phone: this.newReport.contactPersonPhone,
        Position: this.newReport.contactPersonPosition,
        CompanyFullName: this.newReport.applicantFullName,
        CompanyShortName: this.newReport.applicantShortName,
        PostAddress: this.newReport.applicantPostAddress,
        InternetAddress: this.newReport.applicantInternetAddress,
        ShortReportDescription: this.newReport.shortReportDescription,
        ShortCompanyDescription: this.newReport.shortCompanyDescription,
        HelpSites: this.newReport.helpSites,
        OtherSector: this.selectedSector === 0 ? this.otherSector : undefined,
        Nominations: nominations,
        EngReport: (this.selectedReports.find(item => item === 1))
          ? this.reportCategories[1].documentLink
          : undefined,
        RusReport: (this.selectedReports.find(item => item === 0))
          ? this.reportCategories[0].documentLink
          : undefined,
        OnlineReport: (this.selectedReports.find(item => item === 2))
          ? this.reportCategories[2].documentLink
          : undefined,
        SocialRusReport: (this.selectedReports.find(item => item === 3))
          ? this.reportCategories[3].documentLink
          : undefined,
        SocialEngReport: (this.selectedReports.find(item => item === 4))
          ? this.reportCategories[4].documentLink
          : undefined
      }

      return report
    },
    addReport: function (event) {
      event.preventDefault()
      let vm = this
      console.log('add report')
      if (this.submitAvailable && validate(this)) {
        let report = this.prepareNewReport()
        addReport(report, this.captchaResponse).then(
          result => {
            vm.submitAvailable = false
            if (result) {
              console.log('report was added successfully')
            } else {
              console.log('error while adding the report')
              vm.$refs.captcha.reset()
            }
          }
        )
      }
    },
    verifyReCaptcha: function (response) {
      this.captchaResponse = response
      this.submitAvailable = true
    },
    captchaExpired: function () {
      this.captchaResponse = undefined
      this.submitAvailable = false
    }
  }
}
