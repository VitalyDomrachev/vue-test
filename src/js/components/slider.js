import Img from '@/vue/components/Img.vue'
import Swiper from 'swiper/dist/js/swiper'
import {getIsReportAvailable} from '@/js/services/yearService.js'
import $ from 'jquery'

export default {
  mounted () {
    this.$nextTick(function () {
      let $container = $(this.$refs.sliderContainer)
      let $pagination = $(this.$refs.sliderPagination)
      let $prev
      let $next
      if (this.isGallery) {
        $prev = $(this.$refs.sliderPrev)
        $next = $(this.$refs.sliderNext)
      }
      let options = {
        pagination: {
          el: $pagination,
          clickable: true
        },
        navigation: {
          prevEl: $prev,
          nextEl: $next
        },
        autoplay: {
          delay: 5000
        },
        effect: 'fade'
      }

      if (this.isGallery) {
        Object.assign(options, { fadeEffect: { crossFade: true } })
      }

      this.swiper = new Swiper($container[0], options)
      this.swiper.autoplay.stop()
    })
  },
  data () {
    return {
      isSubmitAvailable: getIsReportAvailable(),
      previousSlideLink: 'javascript:void(0);',
      nextSlideLink: 'javascript:void(0);',
      submitView: 'Request',
      submitText: 'Оформить заявку на участие'
    }
  },
  props: {
    isGallery: {
      type: Boolean,
      required: false
    },
    slides: {
      type: Array,
      required: true
    }
  },
  components: {
    Img
  },
  methods:
  {
    start () {
      this.swiper.update()
      this.swiper.autoplay.start()
    },
    stop () {
      this.swiper.slideTo(0, 0, false)
      this.swiper.autoplay.stop()
    }
  }
}
