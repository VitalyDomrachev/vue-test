import Img from '@/vue/components/Img.vue'

export default {
  data () {
    return {
      fullNameColumnTitle: 'ФИО члена жюри',
      organizationAndTitleColumnTitle: 'Должность, Организация'
    }
  },
  props: {
    juryStructure: {
      type: Array,
      required: true
    }
  },
  components: {
    Img
  }
}
