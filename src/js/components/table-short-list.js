export default {
  data () {
    return {
      nominationColumnTitle: 'Наименование номинации',
      winnersColumnTitle: 'Номинанты'
    }
  },
  props: {
    nominations: {
      type: Array,
      required: true
    }
  }
}
