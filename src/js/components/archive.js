export default {
  props: {
    title: {
      type: String,
      required: true
    },
    items: {
      type: Array,
      required: true
    },
    path: {
      type: String,
      required: true
    }
  },
  methods: {
    getLink: function (item) {
      return this.path + '?year=' + item.year
    }
  }
}
