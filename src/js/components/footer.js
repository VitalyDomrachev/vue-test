import Img from '../../vue/components/Img.vue'
import {getMenu} from '@/js/services/menuService.js'

export default {
  data () {
    return {
      msg: 'Welcome to Your Vue.js App 2',
      contactsTitle: 'КОНТАКТНАЯ ИНФОРМАЦИЯ',
      contacts: [
        'Департамент клиентской поддержки:',
        '(495) 363-32-32, доб.25-042',
        'Богданова Ирина'
      ],
      emailTitle: 'e-mail: ',
      email: 'ar@moex.com',
      copyright: 'Copyright © Московская Биржа, 2008 - ' + (new Date()).getFullYear() + '. Все права защищены.',
      ratingTitle: 'Присоединяйтесь к нам:',
      ratingLink: 'javascript:void(0);',
      ratingImage: '/static/rating.png',
      menu: getMenu(),
      socialLinks: [
        { link: 'https://twitter.com/moscow_exchange', class: '_twitter' },
        { link: 'https://www.facebook.com/MSKExchange', class: '_facebook' },
        { link: 'https://instagram.com/moscow_exchange/', class: '_instagram' }
      ]
    }
  },
  components: { Img }
}
