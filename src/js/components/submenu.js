import {getSubmenu, getSelectedSubmenuId} from '@/js/services/menuService.js'

export default {
  computed: {
    submenu () {
      return getSubmenu()
    },
    selectedSubmenuId () {
      return getSelectedSubmenuId()
    }
  }
}
