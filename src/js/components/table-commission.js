export default {
  data () {
    return {
      fullNameColumnTitle: 'ФИО члена жюри',
      organizationAndTitleColumnTitle: 'Должность, Организация'
    }
  },
  props: {
    commissionStructure: {
      type: Array,
      required: true
    }
  }
}
