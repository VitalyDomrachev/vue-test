export default {
  props: {
    title: {
      type: String,
      required: true
    },
    mods: {
      type: Array,
      required: false
    }
  }
}
