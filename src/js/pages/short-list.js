import Banner from '@/vue/components/Banner.vue'
import Accordion from '@/vue/components/Accordion.vue'
import TableShortList from '@/vue/components/TableShortList.vue'
import Archive from '@/vue/components/Archive.vue'
import {getNominations} from '@/js/services/nominationsService.js'
import {getFilteredYears, romanize} from '@/js/services/yearService.js'
import scrollToElement from 'scroll-to-element'

export default {
  data () {
    return {
      bannerUrl: '/static/winners-banner.jpg',
      secondContentTitle: 'Шорт-листы номинаций',
      archiveTitle: 'Архив шорт-листов конкурса',
      shortList: [],
      archiveShortLists: [],
      title: 'Номинанты, вошедшие в шорт-листы основных номинаций'
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.getShortLists(this.year)
  },
  methods: {
    refreshShortLists (year) {
      this.shortList = this.archiveShortLists.find(item => item.year === Number.parseInt(this.year)).shortLists
      this.secondContentTitle = this.getTitle(this.year)
    },
    getTitle (year) {
      return 'Шорт-листы ' + romanize(year) + ' Ежегодного конкурса годовых отчетов'
    },
    getShortLists () {
      getNominations().then(result => {
        if (result) {
          let archive = getFilteredYears()
            .map(item => {
              return {
                content: this.getTitle(item),
                year: item,
                shortLists: this.formatShortList(result[item].nominations)
              }
            })
          this.archiveShortLists = archive
          this.refreshShortLists(this.year)
        }
      })
    },
    formatShortList (nominations) {
      if (!nominations.mainNominations || !nominations.mainNominations.items) return undefined
      let rawNominations = []
      for (let sponsorId in nominations.mainNominations.items) {
        rawNominations = rawNominations.concat(...nominations.mainNominations.items[sponsorId].nominations)
      }
      return rawNominations.map(nomination => {
        let nominees = (nomination.shortList || []).map(winner => winner.name)
        return {
          nomination: nomination.fullName,
          nominees: nominees
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      scrollToElement('.second-content__inner', {duration: 500})
      this.refreshShortLists(newVal)
    }
  },
  components: {
    Banner,
    Accordion,
    Archive,
    TableShortList
  }
}
