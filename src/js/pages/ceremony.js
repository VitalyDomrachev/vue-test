import Banner from '@/vue/components/Banner.vue'
import Archive from '@/vue/components/Archive.vue'
import {getCeremony} from '@/js/services/ceremonyService.js'
import scrollToElement from 'scroll-to-element'
import Loading from '@/vue/components/Loading.vue'

export default {
  data () {
    return {
      bannerUrl: '/static/about-banner.jpg',
      ceremony: [],
      archiveTitle: 'Архив церемоний награждения',
      archiveCeremony: [],
      htmlContent: '',
      dataLoading: true
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.refreshCeremony(this.year)
  },
  methods: {
    refreshCeremony (year) {
      let self = this
      self.dataLoading = true
      getCeremony(year).then(result => {
        self.dataLoading = false
        if (result) {
          self.htmlContent = result.find(item => item.year === year).htmlContent
          self.archiveCeremony = result.sort((a, b) => b.year - a.year)
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      scrollToElement('.second-content__inner', {duration: 500})
      this.htmlContent = this.archiveCeremony.find(item => item.year === newVal).htmlContent
    }
  },
  components: {
    Banner,
    Archive,
    Loading
  }
}
