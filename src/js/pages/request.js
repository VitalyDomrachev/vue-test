import Banner from '@/vue/components/Banner.vue'
import FormRequest from '@/vue/components/FormRequest.vue'

export default {
  data () {
    return {
      bannerUrl: '/static/about-banner.jpg',
      secondContentTitleHtml: 'Анкета-заявление<br> на участие в Конкурсе'
    }
  },
  components: {
    Banner,
    FormRequest
  }
}
