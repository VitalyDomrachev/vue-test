import Banner from '@/vue/components/Banner.vue'
import Accordion from '@/vue/components/Accordion.vue'
import Archive from '@/vue/components/Archive.vue'
import TableWinners from '@/vue/components/TableWinners.vue'
import {getNominations} from '@/js/services/nominationsService.js'
import {romanize, getFilteredYears} from '@/js/services/yearService.js'
import scrollToElement from 'scroll-to-element'
import $ from 'jquery'

export default {
  data () {
    return {
      bannerUrl: '/static/winners-banner.jpg',
      secondContentTitle: '',
      nominations: [],
      archiveTitle: 'Архив победителей',
      archiveWinners: []
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.getWinners()
  },
  methods: {
    refreshWinners (year) {
      this.nominations = this.archiveWinners.find(item => item.year === Number.parseInt(this.year)).nominations
      this.secondContentTitle = this.getTitle(this.year)
    },
    getTitle (year) {
      return 'Победители ' + romanize(year) + ' Ежегодного конкурса годовых отчетов ' + year + ' года'
    },
    getWinners () {
      getNominations().then(result => {
        if (result) {
          let archive = getFilteredYears()
            .map(item => {
              return {
                content: this.getTitle(item),
                year: item,
                nominations: this.formatWinners(result[item].nominations)
              }
            })
          this.archiveWinners = archive
          this.refreshWinners(this.year)
        }
      })
    },
    formatWinners (nominations) {
      let nominationsGroups = []
      if (nominations.mainNominations && nominations.mainNominations.items) nominationsGroups.push(nominations.mainNominations)
      if (nominations.additionalNominations && nominations.additionalNominations.items) nominationsGroups.push(nominations.additionalNominations)
      if (nominations.sectorNominations && nominations.sectorNominations.items) nominationsGroups.push(nominations.sectorNominations)
      for (let nominationGroupId in nominations.otherNominations) {
        nominationsGroups.push(nominations.otherNominations[nominationGroupId])
      }
      return nominationsGroups.map(group => {
        let rawNominations = []
        for (let sponsorId in group.items) {
          rawNominations = rawNominations.concat(...group.items[sponsorId].nominations)
        }
        return {
          name: group.title,
          winners: rawNominations.map(nomination => {
            let winnersByPlace = [
              {names: (nomination.winnersFirstPlace || []).map(winner => winner.name), winner: true},
              {names: (nomination.winnersSecondPlace || []).map(winner => winner.name), winner: false},
              {names: (nomination.winnersThirdPlace || []).map(winner => winner.name), winner: false}
            ].filter(item => item.names.length > 0)
            return {
              nomination: nomination.fullName,
              items: winnersByPlace
            }
          })
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      $('.collapse').collapse('hide')
      scrollToElement('.second-content__inner', {duration: 500})
      this.refreshWinners(newVal)
    }
  },
  components: {
    Banner,
    Accordion,
    Archive,
    TableWinners
  }
}
