import Banner from '@/vue/components/Banner.vue'
import Archive from '@/vue/components/Archive.vue'
import {getCommission} from '@/js/services/commissionService.js'
import scrollToElement from 'scroll-to-element'
import Loading from '@/vue/components/Loading.vue'
import $ from 'jquery'

export default {
  data () {
    return {
      bannerUrl: '/static/about-banner.jpg',
      commission: [],
      archiveTitle: 'Архив состава конкурсной комиссии',
      archiveCommission: [],
      htmlContent: '',
      dataLoading: true
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.refreshCommission(this.year)
  },
  methods: {
    refreshCommission (year) {
      let self = this
      self.dataLoading = true
      getCommission(year).then(result => {
        self.dataLoading = false
        if (result) {
          self.htmlContent = result.find(item => item.year === year).htmlContent
          self.archiveCommission = result.sort((a, b) => b.year - a.year)
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      $('.collapse').collapse('hide')
      scrollToElement('.second-content__inner', {duration: 500})
      this.htmlContent = this.archiveCommission.find(item => item.year === newVal).htmlContent
    }
  },
  components: {
    Banner,
    Archive,
    Loading
  }
}
