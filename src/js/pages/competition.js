import Banner from '@/vue/components/Banner.vue'
import Img from '@/vue/components/Img.vue'
import {getCompetition} from '@/js/services/competitionService.js'

export default {
  data () {
    return {
      bannerUrl: '/static/competition-banner.jpg',
      competitionImageUrl: '/static/competition.jpg',
      secondContentTitle: '',
      htmlBlock: ``
    }
  },
  created () {
    getCompetition().then(result => {
      this.htmlBlock = result.htmlContent
      this.secondContentTitle = result.title
    })
  },
  components: {
    Banner,
    Img
  }
}
