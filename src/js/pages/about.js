import Banner from '@/vue/components/Banner.vue'
import {getAbout} from '@/js/services/aboutService.js'

export default {
  data () {
    return {
      bannerUrl: '/static/about-banner.jpg',
      secondContentTitle: '',
      htmlBlock: ``
    }
  },
  created () {
    getAbout().then(result => {
      this.htmlBlock = result.htmlContent
      this.secondContentTitle = result.title
    })
  },
  components: {
    Banner
  }
}
