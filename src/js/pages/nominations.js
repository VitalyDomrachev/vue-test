import Banner from '@/vue/components/Banner.vue'
import TableNominationsMain from '@/vue/components/TableNominationsMain.vue'
import TableNominationsAdditional from '@/vue/components/TableNominationsAdditional.vue'
import TableNominationsBestReport from '@/vue/components/TableNominationsBestReport.vue'
import Archive from '@/vue/components/Archive.vue'
import Loading from '@/vue/components/Loading.vue'
import {getNominations} from '@/js/services/nominationsService.js'
import scrollToElement from 'scroll-to-element'

export default {
  data () {
    return {
      bannerUrl: '/static/nominations-banner.jpg',
      mainNominationsTitleHtml: 'Основные номинации<br> XX Ежегодного конкурса',
      additionalNominationsTitleHtml: 'Дополнительные номинации<br> XX Ежегодного конкурса',
      bestReportNominationsTitleHtml: 'Лучший годовой отчет отрасли',
      archiveTitle: 'Архив номинаций конкурса',
      nominations: undefined,
      archiveNominations: [],
      pagePath: '/about/nominations',
      dataLoading: true
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.refreshNominations(this.year)
  },
  methods: {
    refreshNominations (year) {
      let self = this
      self.dataLoading = true
      getNominations().then(result => {
        self.dataLoading = false
        if (result) {
          self.nominations = result[year].nominations
          let archiveNominations = []
          for (let yearItem in result) {
            archiveNominations.push({content: result[yearItem].title, year: yearItem})
          }
          self.archiveNominations = archiveNominations.sort((a, b) => b.year - a.year)
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      scrollToElement('.second-content__inner', {duration: 500})
      this.refreshNominations(newVal)
    }
  },
  components: {
    Banner,
    TableNominationsBestReport,
    TableNominationsMain,
    TableNominationsAdditional,
    Archive,
    Loading
  }
}
