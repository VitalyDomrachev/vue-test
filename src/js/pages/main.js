import Reports from '@/vue/components/reports/Reports.vue'
import Slider from '@/vue/components/Slider.vue'
import {getMainSlidesContent} from '@/js/services/staticContentService.js'

export default {
  data () {
    return {
      slides: []
    }
  },
  props: {
    type: {
      type: String,
      required: false,
      default: 'list'
    },
    year: {
      type: String,
      required: false
    },
    category: {
      type: String,
      required: false,
      default: ''
    },
    categoryId: {
      type: String,
      required: false,
      default: ''
    },
    fullPath: {
      type: String,
      required: false,
      default: ''
    },
    page: {
      type: String,
      required: false,
      default: '1'
    }
  },
  created () {
    getMainSlidesContent().then(result => {
      this.slides = result
      this.$nextTick(function () {
        this.$refs.slider.start()
      })
    })
  },
  components: {
    Reports,
    Slider
  }
}
