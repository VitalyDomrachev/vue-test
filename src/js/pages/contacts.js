import Banner from '@/vue/components/Banner.vue'
import Img from '@/vue/components/Img.vue'
import {getContacts} from '@/js/services/contactsService.js'

export default {
  data () {
    return {
      bannerUrl: '/static/about-banner.jpg',
      competitionImageUrl: '/static/competition.jpg',
      secondContentTitle: '',
      htmlBlock: ``
    }
  },
  created () {
    getContacts().then(result => {
      this.htmlBlock = result.htmlContent
      this.secondContentTitle = result.title
      this.$nextTick(function () {
        let script = document.createElement('script')
        let $element = this.$refs['placeholder']
        script.setAttribute('src', 'https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3A5e0a3c40ef1f55bdf63bea5f364643436952929496055837dabd533a7a57fe7f&amp;width=100%25&amp;height=400&amp;lang=ru_RU&amp;scroll=true')
        script.setAttribute('type', 'text/javascript')
        script.setAttribute('charset', 'utf-8')
        script.setAttribute('async', '')
        $element.append(script)
      })
    })
  },
  components: {
    Banner,
    Img
  }
}
