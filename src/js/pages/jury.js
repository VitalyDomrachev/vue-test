import Banner from '@/vue/components/Banner.vue'
import Accordion from '@/vue/components/Accordion.vue'
import Archive from '@/vue/components/Archive.vue'
import TableJury from '@/vue/components/TableJury.vue'
import {getJury} from '@/js/services/juryService.js'
import scrollToElement from 'scroll-to-element'
import Loading from '@/vue/components/Loading.vue'
import $ from 'jquery'

export default {
  data () {
    return {
      bannerUrl: '/static/jury-banner.jpg',
      secondContentTitle: 'Состав Жюри XX Ежегодного конкурса годовых отчетов 2017 года',
      accordionTitle: 'Состав Жюри XX Ежегодного конкурса',
      jury: [],
      fullData: {},
      archiveTitle: 'Архив состава жюри',
      archiveJury: [],
      description: '',
      dataLoading: true
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    let self = this
    this.dataLoading = true
    getJury(this.year).then(result => {
      this.dataLoading = false
      if (result) {
        self.fullData = result
        self.archiveJury = Object.keys(result).map(item => {
          return {
            content: result[item].archiveTitle,
            year: item
          }
        }).sort((a, b) => b.year - a.year)
        self.refreshJury(self.year)
      }
    })
  },
  methods: {
    refreshJury (year) {
      this.jury = this.fullData[year].jury
      this.secondContentTitle = this.fullData[year].title1
      this.accordionTitle = this.fullData[year].title2
      this.description = this.fullData[year].description
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      $('.collapse').collapse('hide')
      scrollToElement('.second-content__inner', {duration: 500})
      this.refreshJury(newVal)
    }
  },
  components: {
    Banner,
    Accordion,
    Archive,
    TableJury,
    Loading
  }
}
