import Banner from '@/vue/components/Banner.vue'
import Archive from '@/vue/components/Archive.vue'
import Loading from '@/vue/components/Loading.vue'
import {getDocuments} from '@/js/services/documentsService.js'
import scrollToElement from 'scroll-to-element'
import $ from 'jquery'

export default {
  data () {
    return {
      bannerUrl: '/static/documents-banner.jpg',
      archiveTitle: 'Архив документов',
      htmlContent: '',
      archiveDocuments: [],
      documents: [],
      dataLoading: true
    }
  },
  props: {
    year: {
      type: String,
      required: false
    },
    path: {
      type: String,
      required: false,
      default: ''
    }
  },
  created () {
    this.refreshDocuments(this.year)
  },
  methods: {
    refreshDocuments (year) {
      let self = this
      this.dataLoading = true
      getDocuments(year).then(result => {
        self.dataLoading = false
        if (result) {
          self.htmlContent = result.find(item => item.year === year).htmlContent
          self.archiveDocuments = result.sort((a, b) => b.year - a.year)
        }
      })
    }
  },
  watch: {
    year: function (newVal, oldVal) {
      $('.collapse').collapse('hide')
      scrollToElement('.second-content__inner', {duration: 500})
      this.htmlContent = this.archiveDocuments.find(item => item.year === newVal).htmlContent
    }
  },
  components: {
    Banner,
    Archive,
    Loading
  }
}
