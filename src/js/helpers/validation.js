import 'parsleyjs'
import 'parsleyjs/dist/i18n/ru'
import 'parsleyjs/dist/i18n/ru.extra'
import $ from 'jquery'
/* eslint-disable */

function initializeValidation (vm) {
  $(vm.$el).parsley().reset()
  // перезапись дефолтных опций parsley
  Object.assign(Parsley.options, {
    errorClass: 'is-invalid',
    successClass: 'is-valid',
    excluded: 'input[type=button], input[type=submit], input[type=reset], input[type=hidden], [disabled], :hidden',
    errorsMessagesDisabled: true,
    classHandler (ParsleyField) {
      let $elements = ParsleyField.$element

      if (ParsleyField.$elements) {
        $elements = ParsleyField.$elements.reduce($.merge)
      }

      return $elements
    }
  })
}

function validate (vm) {
  let isValid = $(vm.$el).parsley().isValid()
  return isValid
}

export {initializeValidation, validate}
